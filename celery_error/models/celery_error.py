import importlib

from django.db import models
from pydoc import locate


class CeleryError(models.Model):
    task_id = models.CharField('Task ID', max_length=255, unique=True, )
    task = models.CharField('Task', max_length=255)
    args = models.TextField(verbose_name='ARGS', null=True, blank=True)
    kwargs = models.TextField(verbose_name='KWARGS', null=True, blank=True)
    einfo = models.TextField(verbose_name='EINFO', null=True, blank=True)
    traceback = models.TextField('Fehler', blank=True, null=True)
    created = models.DateTimeField('Zeitpunk', auto_now=True)

    class Meta:
        """Table information."""
        verbose_name = 'Celery Fehler'
        verbose_name_plural = 'Celery Fehler'

    def __str__(self):
        return '<Task: {0.task_id} ({0.task})>'.format(self)

    def rerun(self):
        print(self.task)
        module_name, method_name = self.task.rsplit('.', 1)
        module = importlib.import_module(module_name)
        method = getattr(module, method_name)
        method.apply_async(
            args=eval(self.args),
            kwargs=eval(self.kwargs)
        )
        self.delete()

