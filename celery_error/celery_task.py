import rollbar
from celery import Task

from hogapage.hp_global.celery_error.models import CeleryError


class ErrorTask(Task):
    raise_rollbar_error = True

    def on_failure(self, exc, task_id, args, kwargs, einfo):
        CeleryError.objects.create(
            task_id=task_id,
            task=self.name,
            args=args,
            kwargs=kwargs,
            traceback=einfo,
            einfo=exc
        )

        if self.raise_rollbar_error:
            rollbar.report_message(str(einfo), extra_data={
                'name': self.name,
                'task_id': task_id,
                'args': args,
                'kwargs': kwargs,
                'exc': str(exc),
            })
