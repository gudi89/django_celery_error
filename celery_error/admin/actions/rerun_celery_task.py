from django.contrib import messages


def rerun_celery_task(modeladmin, request, queryset):
    print(queryset)
    for item in queryset:
        print(item)
        item.rerun()

    messages.add_message(request, messages.SUCCESS, 'Die Tasks wurden erneut ausgeführt.', extra_tags='stay')


rerun_celery_task.short_description = "Tasks erneut ausführen"

