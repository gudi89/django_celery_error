from django.contrib import admin

from hogapage.hp_admin.admin import global_admin
from hogapage.hp_global.celery_error.admin.actions.rerun_celery_task import rerun_celery_task
from ..models import CeleryError


@admin.register(CeleryError, site=global_admin)
class CeleryErrorAdmin(admin.ModelAdmin):
    list_display = ('task_id', 'task', 'created', 'einfo')
    list_filter = ('task', 'einfo')
    actions = [rerun_celery_task]
